/**
 * Created by Ben Yip on 2015/1/6.
 */

/**
 * automatically hide feedback message
 */
$(function hideAlert() {
    var alert_message = $(".alert.feedback-float");
    var timeoutID = setTimeout(function () {
        alert_message.fadeOut();
    }, 1500);
    alert_message.mouseenter(function () {
        clearTimeout(timeoutID);
    }).mouseleave(function () {
        $(this).fadeOut();
    });
});

/**
 * test if is undefined
 */
function isUndefined(o) {
    return typeof(o) == "undefined";
}

/**
 * see if it's IE
 */
function isIE() {
    var userAgent = navigator.userAgent,
        rMsie = /(msie\s|trident.*rv:)([\w.]+)/,
        rFirefox = /(firefox)\/([\w.]+)/,
        rOpera = /(opera).+version\/([\w.]+)/,
        rChrome = /(chrome)\/([\w.]+)/,
        rSafari = /version\/([\w.]+).*(safari)/;
    var browser;
    var version;
    var ua = userAgent.toLowerCase();

    function uaMatch(ua) {
        var match = rMsie.exec(ua);
        if (match != null) {
            return {browser: "IE", version: match[2] || "0"};
        }
        match = rFirefox.exec(ua);
        if (match != null) {
            return {browser: match[1] || "", version: match[2] || "0"};
        }
        match = rOpera.exec(ua);
        if (match != null) {
            return {browser: match[1] || "", version: match[2] || "0"};
        }
        match = rChrome.exec(ua);
        if (match != null) {
            return {browser: match[1] || "", version: match[2] || "0"};
        }
        match = rSafari.exec(ua);
        if (match != null) {
            return {browser: match[2] || "", version: match[1] || "0"};
        }
        if (match != null) {
            return {browser: "", version: "0"};
        }
    }

    var browserMatch = uaMatch(userAgent.toLowerCase());
    if (browserMatch.browser) {
        browser = browserMatch.browser;
        version = browserMatch.version;
    }

    return browserMatch.browser.toLowerCase().match("ie");
}

/**
 * given a number or string represents seconds.
 * return-pattern 12:09:34
 * @param second
 * @returns {string}
 */
function toHHMMSS(second) {
    var time = parseFloat(second.toString());
    var hh = Math.floor(time / 3600);
    var mm = Math.floor(time / 60 - hh * 60);
    var ss = Math.round(time - hh * 3600 - mm * 60);
    return (hh === 0 ? "00:" : hh <= 9 ? ("0" + hh + ":") : hh + ":")
        + (mm === 0 ? "00:" : mm + ":")
        + (ss === 0 ? "00:" : ss);
}

/**
 * given a number or string represents seconds.
 * return-pattern 26'12"
 * the minutes would be returned as empty string if equals zero.
 * @param second
 * @returns {string}
 */
function toMMSS(second) {
    var time = parseFloat(second.toString());
    var mm = Math.floor(time / 60);
    var ss = Math.round(time - mm * 60);
    return (mm > 9 ? mm + "\'" : mm === 0 ? "" : ("0" + mm + "\'" ))
        + (ss <= 9 ? ("0" + ss + "\"" ) : ss + "\"");
}

/**
 * 计算字符串所占的内存字节数，默认使用UTF-8的编码方式计算，也可制定为UTF-16
 * UTF-8 是一种可变长度的 Unicode 编码格式，使用一至四个字节为每个字符编码
 *
 * 000000 - 00007F(128个代码)      0zzzzzzz(00-7F)                             一个字节
 * 000080 - 0007FF(1920个代码)     110yyyyy(C0-DF) 10zzzzzz(80-BF)             两个字节
 * 000800 - 00D7FF
   00E000 - 00FFFF(61440个代码)    1110xxxx(E0-EF) 10yyyyyy 10zzzzzz           三个字节
 * 010000 - 10FFFF(1048576个代码)  11110www(F0-F7) 10xxxxxx 10yyyyyy 10zzzzzz  四个字节
 *
 * 注: Unicode在范围 D800-DFFF 中不存在任何字符
 * {@link http://zh.wikipedia.org/wiki/UTF-8}
 *
 * UTF-16 大部分使用两个字节编码，编码超出 65535 的使用四个字节
 * 000000 - 00FFFF  两个字节
 * 010000 - 10FFFF  四个字节
 *
 * {@link http://zh.wikipedia.org/wiki/UTF-16}
 * @param  {String} str
 * @param  {String} charset utf-8, utf-16
 * @return {Number}
 */
function getByteLength(str, charset){
    var total = 0,
        charCode,
        i,
        len;

    charset = charset ? charset.toLowerCase() : '';
    if(charset === 'utf-16' || charset === 'utf16'){
        for(i = 0, len = str.length; i < len; i++){
            charCode = str.charCodeAt(i);
            if(charCode <= 0xffff){
                total += 2;
            }else{
                total += 4;
            }
        }
    }else{
        for(i = 0, len = str.length; i < len; i++){
            charCode = str.charCodeAt(i);
            if(charCode <= 0x007f) {
                total += 1;
            }else if(charCode <= 0x07ff){
                total += 2;
            }else if(charCode <= 0xffff){
                total += 3;
            }else{
                total += 4;
            }
        }
    }
    return total;
}


/**
 * evoke the masonry layout.
 * This should be called once the document is ready.
 *
 * If there is audio or video,
 * this should be call again when the media is loaded.
 */
function evokeMasonryLayout() {
    var $masonry = $(".masonry-container");

    $masonry.masonry({
        itemSelector: ".mason",
        isAnimated: true
    });

    $(window).resize(function () {
        $masonry.masonry("reload");
    });
}

/**
 * activate tooltips
 */
$(function () {
    $("[data-toggle='tooltip']").tooltip();
});