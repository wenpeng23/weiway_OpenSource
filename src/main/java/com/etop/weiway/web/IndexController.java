package com.etop.weiway.web;

import com.etop.weiway.basic.controller.BaseController;
import com.etop.weiway.commons.util.Encrypt;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pengo.Wen
 * Created by pengo on 14-9-19.
 */
@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = "/wxmanager/ueditor", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String ueditor(){
        return "/global/ueditor/wechatEditor";
    }
}
