package com.etop.weiway.commons.annotation;

import java.lang.annotation.*;

/**
 * Created by KyLeo on 2015/1/29.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccountRequired {
}
