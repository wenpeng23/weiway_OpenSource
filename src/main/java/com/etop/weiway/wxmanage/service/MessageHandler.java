package com.etop.weiway.wxmanage.service;

import com.etop.weixin.entity.req.BaseReqEntity;

import javax.servlet.http.HttpSession;

/**
 * Created by Jeremie on 2014/12/30.
 */
public interface MessageHandler {

    public String replyMessage(BaseReqEntity reqEntity, String wechatId) throws Exception;

    public String replyMessageBySession(BaseReqEntity reqEntity, HttpSession session, String wechatId) throws Exception;
}
