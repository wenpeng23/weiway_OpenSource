package com.etop.weiway.wxmanage.service;

import com.etop.weiway.basic.service.BaseService;
import com.etop.weiway.wxmanage.dao.*;
import com.etop.weiway.wxmanage.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信时间控制服务类
 * Created by Jeremie on 2015/1/2.
 */
@Service("WeixinTimeService")
public class WeixinTimeService extends BaseService {

    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private MaterialImageDAO materialImageDAO;
    @Autowired
    private MaterialVoiceDAO materialVoiceDAO;
    @Autowired
    private MaterialVideoDAO materialVideoDAO;
    @Autowired
    private MaterialMutinewsDAO materialMutinewsDAO;

    /**
     * 更新公众号accessToken
     * @param account
     */
    public void updateAccountAccessToken(Account account)throws Exception {
        log.debug("******更新公众号accessToken******");
        accountDAO.update(account);
    }

    /**
     * 更新图片素材mediaId
     * @param materialImage
     */
    public void updateMaterialImageMediaId(MaterialImage materialImage){
        log.debug("******更新图片素材mediaId******");
        materialImageDAO.update(materialImage);
    }


    /**
     * 更新音频素材mediaId
     * @param materialVoice
     */
    public void updateMterialVoiceMediaId(MaterialVoice materialVoice){
        log.debug("******更新音频素材mediaId******");
        materialVoiceDAO.update(materialVoice);
    }

    /**
     * 更新视频素材mediaId
     * @param materialVideo
     */
    public void updateMaterialVideoMediaId(MaterialVideo materialVideo){
        log.debug("******更新视频素材mediaId******");
        materialVideoDAO.update(materialVideo);
    }
    
    public void updateMaterialMultinewsMediaId(MaterialMutinews materialMutinews){
        log.debug("******更新视频素材mediaId******");
        materialMutinewsDAO.update(materialMutinews);
    }


}
