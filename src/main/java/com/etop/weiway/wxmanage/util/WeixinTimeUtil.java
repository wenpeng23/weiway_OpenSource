package com.etop.weiway.wxmanage.util;

import com.etop.weiway.commons.util.SpringContextHolder;
import com.etop.weiway.wxmanage.entity.*;
import com.etop.weiway.wxmanage.service.WeixinTimeService;
import com.etop.weixin.entity.advanced.WxMedia;
import com.etop.weixin.entity.advanced.WxNews;
import com.etop.weixin.entity.common.AccessToken;
import com.etop.weixin.utils.MassMsgUtil;
import com.etop.weixin.utils.MediaUtil;
import com.etop.weixin.utils.WeixinUtil;

/**
 * 微信时间控制工具
 * Created by Jeremie on 2015/1/1.
 */
public class WeixinTimeUtil {

    private static WeixinTimeService weixinTimeService = SpringContextHolder.getBean("WeixinTimeService");


    /**
     * 获取AccessToken
     *
     * @param account
     * @return
     * @throws Exception
     */
    public static String getAccessToken(Account account) throws Exception {
        if (System.currentTimeMillis() / 1000 - account.getTokenCreatedTime() > 7100l) {
            AccessToken accessToken = WeixinUtil.getAccessToken(account.getAppId(), account.getAppSecret());
            if (accessToken != null) {
                account.setTokenCreatedTime(System.currentTimeMillis() / 1000);
                account.setAccessToken(accessToken.getAccess_token());
                weixinTimeService.updateAccountAccessToken(account);
            } else {
                throw new Exception("获取AccessToken失败");
            }
        }
        return account.getAccessToken();
    }


    /**
     * 获取图片MediaId
     *
     * @param ImagePath
     * @param materialImage
     * @return
     * @throws Exception
     */
    public static String getImageMediaId(String ImagePath, MaterialImage materialImage) throws Exception {
        if (materialImage.getMediaId() == null || "".equals(materialImage.getMediaId())
                || (System.currentTimeMillis() / 1000 - materialImage.getCreatedTime() > 255000l)) {
            WxMedia wxMedia = MediaUtil.uploadMedia(getAccessToken(materialImage.getAccount()), "image", ImagePath);
            if (wxMedia != null) {
                materialImage.setMediaId(wxMedia.getMedia_id());
                materialImage.setCreatedTime(System.currentTimeMillis() / 1000);
                weixinTimeService.updateMaterialImageMediaId(materialImage);
            } else {
                throw new Exception("上传图片失败");
            }
        }
        return materialImage.getMediaId();
    }

    /**
     * 获取缩略图MediaId
     *
     * @param ImagePath
     * @param materialImage
     * @return
     * @throws Exception
     */
    public static String getThumbImageMediaId(String ImagePath, MaterialImage materialImage) throws Exception {
        if ((materialImage.getMediaId() == null) || "".equals(materialImage.getMediaId())
                || (System.currentTimeMillis() / 1000 - materialImage.getCreatedTime() > 255000l)) {
            WxMedia wxMedia = MediaUtil.uploadMedia(getAccessToken(materialImage.getAccount()), "thumb", ImagePath);
            if (wxMedia != null) {
                materialImage.setMediaId(wxMedia.getThumb_media_id());
                materialImage.setCreatedTime(System.currentTimeMillis() / 1000);
                weixinTimeService.updateMaterialImageMediaId(materialImage);
            } else {
                throw new Exception("上传缩略图失败");
            }
        }
        return materialImage.getMediaId();
    }


    /**
     * 获取声音MediaId
     *
     * @param voicePath
     * @param materialVoice
     * @return
     * @throws Exception
     */
    public static String getVoiceMediaId(String voicePath, MaterialVoice materialVoice) throws Exception {
        if (materialVoice.getMediaId() == null || "".equals(materialVoice.getMediaId()) ||
                (System.currentTimeMillis() / 1000 - materialVoice.getCreatedTime() > 255000l)) {
            WxMedia wxMedia = MediaUtil.uploadMedia(getAccessToken(materialVoice.getAccount()), "voice", voicePath);
            if (wxMedia != null) {
                materialVoice.setMediaId(wxMedia.getMedia_id());
                materialVoice.setCreatedTime(System.currentTimeMillis() / 1000);
                weixinTimeService.updateMterialVoiceMediaId(materialVoice);
            } else {
                throw new Exception("上传语音失败");
            }
        }
        return materialVoice.getMediaId();
    }

    /**
     * 获取视频MediaId
     *
     * @param videoPath
     * @param materialVideo
     * @return
     * @throws Exception
     */
    public static String getVideoMediaId(String videoPath, MaterialVideo materialVideo) throws Exception {
        if (materialVideo.getMediaId() == null || "".equals(materialVideo.getMediaId())
                || (System.currentTimeMillis() / 1000 - materialVideo.getCreatedTime() > 255000l)) {
            WxMedia wxMedia = MediaUtil.uploadMedia(getAccessToken(materialVideo.getAccount()), "video", videoPath);
            if (wxMedia != null) {
                materialVideo.setMediaId(wxMedia.getMedia_id());
                materialVideo.setCreatedTime(System.currentTimeMillis() / 1000);
                weixinTimeService.updateMaterialVideoMediaId(materialVideo);
            } else {
                throw new Exception("上传视频失败");
            }
        }
        return materialVideo.getMediaId();
    }

    /**
     * 获取多图文的mediaId
     *
     * @param materialMutinews
     * @param wxNews
     * @param path
     * @return
     * @throws Exception
     */
    public static String getNewsMediaId(MaterialMutinews materialMutinews, WxNews wxNews) throws Exception {
        if (System.currentTimeMillis() / 1000 - materialMutinews.getCreatedTime() > 255000l) {
            WxMedia wxMedia = MediaUtil.uploadNews(getAccessToken(materialMutinews.getAccount()), wxNews);
            if (wxMedia != null) {
                materialMutinews.setMediaId(wxMedia.getMedia_id());
                materialMutinews.setCreatedTime(System.currentTimeMillis() / 1000);
                weixinTimeService.updateMaterialMultinewsMediaId(materialMutinews);
            } else {
                throw new Exception("上传多图文失败");
            }
        }
        return materialMutinews.getMediaId();
    }


    /**
     * 获取视频群发MediaId
     *
     * @param videoPath
     * @param materialVideo
     * @return
     * @throws Exception
     */
    public static String getVideoMassMediaId(String videoPath, MaterialVideo materialVideo) throws Exception {
        return MassMsgUtil.translateVedioId(getAccessToken(materialVideo.getAccount()),
                getVideoMediaId(videoPath, materialVideo), materialVideo.getTitle(), materialVideo.getDescription());
    }
}
