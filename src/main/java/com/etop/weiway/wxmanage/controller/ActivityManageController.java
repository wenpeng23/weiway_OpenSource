package com.etop.weiway.wxmanage.controller;

import com.etop.weiway.basic.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 微活动控制器
 * Created by Jeremie on 2015/1/15.
 */
@Controller
@RequestMapping("/exwxmanager/message")
public class ActivityManageController extends BaseController {
}
