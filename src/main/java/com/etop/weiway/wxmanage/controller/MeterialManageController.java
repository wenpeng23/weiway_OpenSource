package com.etop.weiway.wxmanage.controller;

import com.etop.weiway.basic.controller.BaseController;
import com.etop.weiway.commons.annotation.AccountRequired;
import com.etop.weiway.commons.web.Pager;
import com.etop.weiway.wxmanage.entity.*;
import com.etop.weiway.wxmanage.service.AccountService;
import com.etop.weiway.wxmanage.service.MaterialService;
import com.etop.weixin.entity.common.AccessToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.List;

/**
 * 素材管理控制器
 *
 * @author Jessy
 */
@Controller
@RequestMapping("/wxmanager/material")
public class MeterialManageController extends BaseController {

    @Autowired
    private MaterialService materialService;
    @Autowired
    private AccountService accountService;

    /**
     * 获得当前的公众号id
     *
     * @param session
     * @return
     */
    public Account getCurrentAccount(HttpSession session) {
        Account account = null;
        if (session.getAttribute("currentAccoutId") != null) {
            account = accountService.loadAccount((int) session.getAttribute("currentAccoutId"));
        }
        return account;
    }

    /**
     * 获取text对象
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ModelAttribute(value = "materialText")
    public MaterialText getMaterialText(@RequestParam(required = false) Integer id) throws Exception {
        if (id != null && id > 0) {
            return materialService.loadMaterialText(id);
        } else {
            return new MaterialText();
        }
    }

    /**
     * 跳转到添加素材文本页面
     *
     * @param materialText
     * @param model
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/text/edit", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String addTextForm(@ModelAttribute(value = "materialText") MaterialText materialText, Model model) throws Exception {
        model.addAttribute("materialText", materialText);
        return "home/wxmanage/base/material/text/edit";
    }

    /**
     * 保存素材文本
     *
     * @param materialText
     * @param model
     * @param oldName
     * @param redirectAttributes
     * @param session
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/text/edit", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
    public String addText(MaterialText materialText, Model model, String oldName,
                          RedirectAttributes redirectAttributes, HttpSession session) throws Exception {
        //检测名称是否存在
        if (materialText.getName().equals("") || materialText.getName() == null
                || materialText.getContent().equals("") || materialText.getContent() == null) {
            addMessage(model, ERROR, "文本素材名称或素材内容不能为空！");
            return addTextForm(materialText, model);
        } else if (!oldName.equals(materialText.getName())
                && materialService.loadMaterialText(materialText.getId()) != null) {
            addMessage(model, ERROR, "保存文本素材<" + materialText.getName()
                    + ">失败，文本素材名称已存在！");
            materialText.setName(oldName);
            return addTextForm(materialText, model);
        }
        materialService.saveMaterialText(materialText, getCurrentAccount(session));
        addMessage(redirectAttributes, SUCCESS, "保存文本素材<" + materialText.getName() + ">成功！");
        return "redirect:/wxmanager/material/text/list";
    }

    /**
     * 分页显示文本素材
     *
     * @param materialText
     * @param request
     * @param response
     * @param model
     * @param session
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/text/list", produces = "text/html; charset=utf-8")
    public String listMaterialText(@ModelAttribute(value = "materialText") MaterialText materialText,
                                   HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
        Pager<MaterialText> materialTextPager
                = materialService.getMaterialTextByPager(new Pager<>(request, response), materialText.getContent(), (int) session.getAttribute("currentAccoutId"));
        model.addAttribute("keyword", materialText.getContent());
        model.addAttribute("pager", materialTextPager);
        return "home/wxmanage/base/material/text/list";
    }

    /**
     * 删除文本素材
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/text/delete_{id}", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String deleteText(@PathVariable Integer id) throws Exception {
        //设置为无效相当于删除
        materialService.deleteMaterialText(id);
        return "redirect:/wxmanager/material/text/list";
    }

    /**
     * 注入图片素材信息
     *
     * @param imageId
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ModelAttribute(value = "materialImage")
    public MaterialImage getMaterialImage(@RequestParam(required = false) Integer imageId)
            throws Exception {
        if (imageId != null && imageId > 0) {
            return materialService.loadMaterialImage(imageId);
        } else {
            return new MaterialImage();
        }
    }

    /**
     * 添加或更新图片素材——GET
     *
     * @param materialImage
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/image/edit", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String editImage_get(@ModelAttribute(value = "materialImage") MaterialImage materialImage, Model model) throws Exception {
        model.addAttribute("materialImage", materialImage);
        return "home/wxmanage/base/material/image/edit";
    }

    /**
     * 添加或更新图片素材——POST
     *
     * @param materialImage
     * @param model
     * @param redirectAttributes
     * @param session
     * @param oldName
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/image/edit", method = RequestMethod.POST)
    public String editImage(MaterialImage materialImage, String oldName, Model model,
                            RedirectAttributes redirectAttributes, HttpSession session) throws Exception {
        if ((materialImage.getName() == null || "".equals(materialImage.getName().trim()))
                || (!oldName.equals(materialImage.getName())
                && materialService.loadMaterialImageByName(materialImage.getName(), getCurrentAccount(session).getId()) != null)) {
            addMessage(model, ERROR, "保存图片素材<" + materialImage.getName()
                    + ">失败，图片素材名称为空或已存在！");
            materialImage.setName(oldName);
            return editImage_get(materialImage, model);
        }
        materialService.saveMaterialImage(materialImage, getCurrentAccount(session));
        addMessage(redirectAttributes, SUCCESS, "保存图片素材<" + materialImage.getName() + ">成功！");
        return "redirect:list";
    }

    /**
     * 分页列出图片素材
     *
     * @param materialImage
     * @param request
     * @param response
     * @param model
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/image/list")
    public String listImage(@ModelAttribute(value = "materialImage") MaterialImage materialImage,
                            HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
        Pager<MaterialImage> pager = materialService.getMaterialImageByPage(new Pager<>(request, response), materialImage.getName(), (int) session.getAttribute("currentAccoutId"));
        model.addAttribute("keyword", materialImage.getName());
        model.addAttribute("pager", pager);
        return "home/wxmanage/base/material/image/list";
    }

    /**
     * 删除指定id的图片素材
     *
     * @param id
     * @param redirectAttributes
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/image/delete", method = RequestMethod.GET)
    public String deleteImage(String id, RedirectAttributes redirectAttributes) throws Exception {
        materialService.deleteMaterialImageById(Integer.parseInt(id));
        addMessage(redirectAttributes, SUCCESS, "删除图片素材成功！");
        return "redirect:list";
    }

    /**
     * 注入视频素材信息
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ModelAttribute(value = "materialVideo")
    public MaterialVideo getMaterialVideo(@RequestParam(required = false) Integer id)
            throws Exception {
        if (id != null && id > 0) {
            return materialService.loadMaterialVideo(id);
        } else {
            return new MaterialVideo();
        }
    }

    /**
     * 添加或更新视频素材——GET
     *
     * @param materialVideo
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/video/edit", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String editVideo_get(@ModelAttribute(value = "materialVideo") MaterialVideo materialVideo, Model model) throws Exception {
        model.addAttribute("materialVideo", materialVideo);
        return "home/wxmanage/base/material/video/edit";
    }

    /**
     * 添加或更新视频素材——POST
     *
     * @param materialVideo
     * @param model
     * @param request
     * @param redirectAttributes
     * @param oldName
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/video/edit", method = RequestMethod.POST)
    public String editVideo(MaterialVideo materialVideo,
                            Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, String oldName, HttpSession session) throws Exception {
        if ((materialVideo.getName() == null || "".equals(materialVideo.getName().trim()))
                || (!oldName.equals(materialVideo.getName())
                && materialService.loadMaterialImageByName(materialVideo.getName(), getCurrentAccount(session).getId()) != null)) {
            addMessage(model, ERROR, "保存视频素材<" + materialVideo.getName()
                    + ">失败，视频素材名称为空或者已存在！");
            materialVideo.setName(oldName);
            return editVideo_get(materialVideo, model);
        }
        materialService.saveMaterialVideo(materialVideo, getCurrentAccount(session), Integer.parseInt(request.getParameter("thumbMediaId")));
        addMessage(redirectAttributes, SUCCESS, "保存视频素材<" + materialVideo.getName() + ">成功！");
        return "redirect:list";
    }

    /**
     * 分页列出视频素材
     *
     * @param materialVideo
     * @param request
     * @param response
     * @param model
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/video/list")
    public String listVideo(@ModelAttribute(value = "materialVideo") MaterialVideo materialVideo,
                            HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
        Pager<MaterialVideo> pager = materialService.getMaterialVideoByPage(new Pager<>(request, response),
                materialVideo.getName(), (int) session.getAttribute("currentAccoutId"));
        model.addAttribute("keyword", materialVideo.getName());
        model.addAttribute("pager", pager);
        return "home/wxmanage/base/material/video/list";
    }

    /**
     * 删除指定id的视频素材
     *
     * @param id
     * @param redirectAttributes
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/video/delete", method = RequestMethod.GET)
    public String deleteVideo(String id, RedirectAttributes redirectAttributes) throws Exception {
        materialService.deleteMaterialVideoById(Integer.parseInt(id));
        addMessage(redirectAttributes, SUCCESS, "删除视频素材成功！");
        return "redirect:list";
    }

    /**
     * 注入音频素材信息
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ModelAttribute(value = "materialVoice")
    public MaterialVoice getMaterialVoice(@RequestParam(required = false) Integer id)
            throws Exception {
        if (id != null && id > 0) {
            return materialService.loadMaterialVoice(id);
        } else {
            return new MaterialVoice();
        }

    }

    /**
     * 添加或更新音频素材——GET
     *
     * @param materialVoice
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/voice/edit", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String editVoice(@ModelAttribute(value = "materialVoice") MaterialVoice materialVoice, Model model) throws Exception {
        model.addAttribute("materialVoice", materialVoice);
        return "home/wxmanage/base/material/voice/edit";
    }

    /**
     * 添加或更新音频素材——POST
     *
     * @param materialVoice
     * @param model
     * @param request
     * @param oldName
     * @param redirectAttributes
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/voice/edit", method = RequestMethod.POST)
    public String editVoice(MaterialVoice materialVoice, Model model, HttpServletRequest request,
                            String oldName, RedirectAttributes redirectAttributes, HttpSession session) throws Exception {
        if ((materialVoice.getName() == null || "".equals(materialVoice.getName().trim()))
                || (!oldName.equals(materialVoice.getName())
                && materialService.loadMaterialImageByName(materialVoice.getName(), getCurrentAccount(session).getId()) != null)) {
            addMessage(model, ERROR, "保存音频频素材<" + materialVoice.getName()
                    + ">失败，音频素材名称为空或者已存在！");
            materialVoice.setName(oldName);
            return editVoice(materialVoice, model);
        }
        materialService.saveMaterialVoice(materialVoice, getCurrentAccount(session), Integer.parseInt(request.getParameter("thumbMediaId")));
        addMessage(redirectAttributes, SUCCESS, "保存音频素材<" + materialVoice.getName() + ">成功！");
        return "redirect:list";
    }

    /**
     * 分页列出音频素材
     *
     * @param materialVoice
     * @param request
     * @param response
     * @param model
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/voice/list")
    public String listVoice(@ModelAttribute(value = "materialVoice") MaterialVoice materialVoice,
                            HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
        Pager<MaterialVoice> pager = materialService.getMaterialVoiceByPage(new Pager<>(request, response), materialVoice.getName(), getCurrentAccount(session).getId());
        model.addAttribute("keyword", materialVoice.getName());
        model.addAttribute("pager", pager);
        return "home/wxmanage/base/material/voice/list";
    }

    /**
     * 删除指定id的音频素材
     *
     * @param id
     * @param redirectAttributes
     * @return
     * @throws java.lang.Exception
     */
    @RequiresPermissions("menu:material")
    @AccountRequired
    @RequestMapping(value = "/voice/delete", method = RequestMethod.GET)
    public String deleteVoice(String id, RedirectAttributes redirectAttributes) throws Exception {
        materialService.deleteMaterialVoiceById(Integer.parseInt(id));
        addMessage(redirectAttributes, SUCCESS, "删除音频素材成功！");
        return "redirect:list";
    }

    /**
     * 注入单图文素材信息
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ModelAttribute(value = "materialNews")
    public MaterialNews getMaterialNews(@RequestParam(required = false) Integer id)
            throws Exception {
        if (id != null && id > 0) {
            return materialService.loadMaterialNews(id);
        } else {
            return new MaterialNews();
        }
    }

    /*
     *重新编辑单图文页面的跳转   选择完缩略图图片的跳转
     * 跳转到添加素材单图文页面
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialNews/edit", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String addMaterialNewsForm(@ModelAttribute(value = "materialNews") MaterialNews materialNews) throws Exception {
        return "home/wxmanage/base/material/news/edit";
    }

    /**
     * 添加图文素材
     *
     * @param materialNews
     * @param model
     * @param oldName
     * @param thumbMediaId
     * @param redirectAttributes
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialNews/edit", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
    public String addMaterialNews(MaterialNews materialNews, Model model, String oldName, String portrait, String thumbMediaId,
                                  RedirectAttributes redirectAttributes, HttpSession session) throws Exception {
        if (!oldName.equals(materialNews.getName())
                && materialService.loadMaterialNewsByName(materialNews.getName()) != null) {
            addMessage(model, ERROR, "保存图文素材<" + materialNews.getName()
                    + ">失败，图文素材名称已存在！");
            materialNews.setName(oldName);
            return addMaterialNewsForm(materialNews);
        }
        materialService.saveMaterialNews(materialNews, portrait, thumbMediaId, getCurrentAccount(session));
        addMessage(redirectAttributes, SUCCESS, "保存图文素材<" + materialNews.getName()
                + ">成功。");
        return "redirect:/wxmanager/material/MaterialNews/list";
    }

    /**
     * 删除单图文
     *
     * @param id
     * @param redirectAttributes
     * @return
     * @throws java.lang.Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialNews/delete_{id}", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String deleteMaterialNews(@PathVariable Integer id, RedirectAttributes redirectAttributes) throws Exception {
        materialService.deleteMaterialNews(id);
        addMessage(redirectAttributes, SUCCESS, "删除多图文素材成功。");
        return "redirect:/wxmanager/material/MaterialNews/list";
    }

    /**
     * 列表图文素材
     *
     * @param materialNews
     * @param request
     * @param response
     * @param model
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialNews/list", produces = "text/html; charset=utf-8")
    public String listMaterialNews(@ModelAttribute(value = "materialNews") MaterialNews materialNews,
                                   HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
        Pager<MaterialNews> MaterialNewsPager = materialService.getMaterialNewsByPager(new Pager<>(request, response), materialNews.getName(), getCurrentAccount(session).getId());
        model.addAttribute("keyword", materialNews.getName());
        model.addAttribute("pager", MaterialNewsPager);
        return "home/wxmanage/base/material/news/list";
    }

    /**
     * 注入多图文素材信息
     *
     * @param MutinewsId
     * @return
     * @throws Exception
     */
    @ModelAttribute(value = "materialMutinews")
    public MaterialMutinews getMaterialMutinews(@RequestParam(required = false) Integer MutinewsId)
            throws Exception {
        if (MutinewsId != null && MutinewsId > 0) {
            return materialService.loadMaterialMutinews(MutinewsId);
        } else {
            return new MaterialMutinews();
        }
    }

    /**
     * 跳转添加多图文消息页面
     *
     * @param materialMutinews
     * @param model
     * @return
     * @throws java.lang.Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialMutinews/edit", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String editMaterialMutinewsForm(@ModelAttribute(value = "materialMutinews") MaterialMutinews materialMutinews,
                                           Model model) throws Exception {
        model.addAttribute("materialMutinews", materialMutinews);
        //修改多图文 返回单图文list
        if (materialMutinews.getMultIds() != null && !materialMutinews.getMultIds().equals("")) {
            MaterialNews firstNews = materialService.getMutinews_firstNews(materialMutinews);
            List<MaterialNews> remainNewses = materialService.getMutinews_remainNewses(materialMutinews);
            model.addAttribute("firstNews", firstNews);
            model.addAttribute("remainNewses", remainNewses);
        }
        return "home/wxmanage/base/material/mutinews/edit";
    }

    /**
     * 添加多图文素材Post请求处理 代码太长待修改（异步选择图文）
     *
     * @param materialMutinews
     * @param model
     * @param oldName
     * @param request
     * @param session
     * @param redirectAttributes
     * @return
     * @throws java.lang.Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialMutinews/add", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
    public String editMaterialMutinews(MaterialMutinews materialMutinews, Model model, String oldName,
                                       HttpServletRequest request, HttpSession session, RedirectAttributes redirectAttributes) throws Exception {
        if ((materialMutinews.getName() == null || "".equals(materialMutinews.getName().trim()))
                || (!oldName.equals(materialMutinews.getName())
                && materialService.loadMaterialMutinewsByName(materialMutinews.getName(), getCurrentAccount(session).getId()) != null)) {
            addMessage(model, ERROR, "保存多图文素材<" + materialMutinews.getName()
                    + ">失败，多图文素材名称为空或者已存在！");
            materialMutinews.setName(oldName);
            return editMaterialMutinewsForm(materialMutinews, model);
        }

        //获取到的单图文id
        String newsID = "";
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = paramNames.nextElement();
            if (paramName.startsWith("newsID")) {
                newsID += request.getParameter(paramName);
                newsID += ",";
            }
        }
        materialService.saveMaterialMutinews(materialMutinews, getCurrentAccount(session), newsID);
        addMessage(redirectAttributes, SUCCESS, "保存多图文素材<" + materialMutinews.getName()
                + ">成功。");
        return "redirect:/wxmanager/material/MaterialMutinews/list";
    }

    /**
     * 多图文列表
     *
     * @param materialMutinews
     * @param request
     * @param response
     * @param model
     * @param accessToken
     * @param session
     * @return
     * @throws Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialMutinews/list", produces = "text/html; charset=utf-8")
    public String listMaterialMutinews(@ModelAttribute(value = "materialMutinews") MaterialMutinews materialMutinews,
                                       HttpServletRequest request, HttpServletResponse response, Model model, AccessToken accessToken, HttpSession session) throws Exception {
        Pager<MaterialMutinews> MaterialMutinewsPager = materialService.getMaterialMutinewsByPager(accessToken.getAccess_token(),
                new Pager<>(request, response), materialMutinews.getName(), (int) session.getAttribute("currentAccoutId"));
        model.addAttribute("keyword", materialMutinews.getName());
        model.addAttribute("pager", MaterialMutinewsPager);
        return "home/wxmanage/base/material/mutinews/list";
    }

    /**
     * 删除多图文
     *
     * @param id
     * @param redirectAttributes
     * @return
     * @throws java.lang.Exception
     */
    @AccountRequired
    @RequestMapping(value = "/MaterialMutinews/delete_{id}", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String deleteMaterialMutinews(@PathVariable Integer id, RedirectAttributes redirectAttributes) throws Exception {
        materialService.deleteMaterialMutinews(id);
        addMessage(redirectAttributes, SUCCESS, "删除多图文素材成功。");
        return "redirect:/wxmanager/material/MaterialMutinews/list";
    }

    /**
     * 上传媒体
     *
     * @param multipartFile
     * @param type
     * @param request
     * @param session
     * @param saveMaterial
     * @param isForNews
     * @return
     * @throws java.lang.Exception
     */
    @ResponseBody
    @RequestMapping(value = "/upload_{type}", produces = "text/html; charset=utf-8")
    public String uploadMedia(@RequestParam("mediaFile") MultipartFile multipartFile, @PathVariable String type,
                              HttpServletRequest request, String saveMaterial, String isForNews, HttpSession session) throws Exception {
        String path = request.getServletContext().getRealPath("/");
        return materialService.handleUploadMedia(path, isForNews, ((User) session.getAttribute("user")).getId()
                , (Integer) session.getAttribute("currentAccoutId"), multipartFile, type, saveMaterial);
    }

    /**
     * 删除媒体
     *
     * @param type
     * @param fileName
     * @param request
     * @param session
     * @return
     * @throws java.lang.Exception
     */
    @ResponseBody
    @RequestMapping(value = "/deleteMaterial_{type}", produces = "text/html; charset=utf-8")
    public String deleteMedia(@PathVariable String type, String fileName, HttpServletRequest request, HttpSession session) throws Exception {
        if (session.getAttribute("user") != null) {
            String path = request.getServletContext().getRealPath("/");
            return materialService.handleDelMedia(path, ((User) session.getAttribute("user")).getId(), fileName, type);
        }
        return "error:用户未登陆";
    }

    /**
     * 返回缩略图素材json格式
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/thumbJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String listThumbJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        String json = materialService.getMaterialThumb((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
        return materialService.getMaterialThumb((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }

    /**
     * 获取多图文的json数据
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/multinewsJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String listMaterialMutinewsJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        return materialService.getMaterialMutinews((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }

    /**
     * 获取视频的json数据
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/videoJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String getVideoJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        return materialService.getMaterialVideo((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }

    /**
     * 获取音频的json数据
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/voiceJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String getVoiceJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        return materialService.getMaterialVoice((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }

    /**
     * 获取文本的json数据
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/textJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String getTextJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        return materialService.getMaterialText((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }

    /**
     * 获取图片的json数据
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/imageJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String getImageJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        return materialService.getMaterialImage((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }

    /**
     * 获取单图文的json数据
     *
     * @param session
     * @param startIndex
     * @param pageSize
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:material")
    @ResponseBody
    @RequestMapping(value = "/newsJson", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String listMaterialNewsJson(HttpSession session, int startIndex, int pageSize) throws Exception {
        return materialService.getMaterialNews((Integer) session.getAttribute("currentAccoutId"), startIndex, pageSize);
    }
}
