package com.etop.weiway.wxmanage.controller;

import com.etop.weiway.basic.controller.BaseController;
import com.etop.weiway.commons.annotation.AccountRequired;
import com.etop.weiway.commons.web.Pager;
import com.etop.weiway.wxmanage.entity.AutoReply;
import com.etop.weiway.wxmanage.service.AutoReplyService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 自定义自动回复控制器
 * Created by KyLeo on 2014/12/29.
 */
@Controller
@RequestMapping("/wxmanager")
public class AutoReplyManageController extends BaseController {
    @Autowired
    private AutoReplyService autoReplyService;

    /**
     * 跳转到消息列表页面
     *
     * @return
     */
    @RequiresPermissions("menu:autoReply")
    @RequestMapping(value = "/autoReply/list")
    @AccountRequired
    public String toListPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, String keyWord) throws Exception {
        request.setAttribute("pager", autoReplyService.getAutoReplyListByAccountId(new Pager<>(request, response),
                (Integer) session.getAttribute("currentAccoutId"), keyWord));
        request.setAttribute("search", keyWord);
        return "/home/wxmanage/base/autoReplyManage/list";
    }

    /**
     * 跳转到编辑页面
     *
     * @param request
     * @param operation
     * @param id
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:autoReply")
    @RequestMapping("/autoReply/edit")
    public String toEditPage(HttpServletRequest request, String operation, String id) throws Exception {
        if ("update".equals(operation)) {
            request.setAttribute("autoReply", autoReplyService.findAutoReplyById(Integer.parseInt(id)));
        }
        request.setAttribute("operation", operation);
        return "/home/wxmanage/base/autoReplyManage/edit";
    }

    /**
     * 保存自动回复
     *
     * @param redirectAttributes
     * @param session
     * @param autoReply
     * @param operation
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:autoReply")
    @RequestMapping(value = "/autoReply/editAutoReply")
    public String editAutoReply(RedirectAttributes redirectAttributes, HttpSession session,
                                AutoReply autoReply, String operation) throws Exception {
        if ("add".equals(operation)) {
            autoReplyService.saveAutoReply(autoReply, (Integer) session.getAttribute("currentAccoutId"));
            addMessage(redirectAttributes, SUCCESS, "添加成功 ! ");
        } else {
            if ("update".equals(operation)) {
                autoReplyService.updateAutoReply(autoReply, (Integer) session.getAttribute("currentAccoutId"));
                addMessage(redirectAttributes, SUCCESS, "修改成功 ! ");
            }
        }
        return "redirect:/wxmanager/autoReply/list";
    }

    /**
     * 删除自动回复
     *
     * @param id
     * @param redirectAttributes
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:autoReply")
    @RequestMapping(value = "/autoReply/delete")
    public String deleteAutoReply(String id, RedirectAttributes redirectAttributes) throws Exception {
        autoReplyService.deleteAutoReplyById(Integer.parseInt(id));
        addMessage(redirectAttributes, SUCCESS, "删除成功 ! ");
        return "redirect:/wxmanager/autoReply/list";
    }
}
