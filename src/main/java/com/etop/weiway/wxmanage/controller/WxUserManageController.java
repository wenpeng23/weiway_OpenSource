package com.etop.weiway.wxmanage.controller;

import com.etop.weiway.basic.controller.BaseController;
import com.etop.weiway.commons.annotation.AccountRequired;
import com.etop.weiway.commons.web.Pager;
import com.etop.weiway.wxmanage.entity.WeixinUser;
import com.etop.weiway.wxmanage.service.WxUserManageService;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLDecoder;

/**
 * 微信用户管理控制器
 * Created by Jeremie on 2015/1/15.
 */
@Controller
@RequestMapping("/wxmanager/wxuser")
public class WxUserManageController extends BaseController {
    @Autowired
    private WxUserManageService wxUserManageService;

    /**
     * 跳转到微信用户列表页面
     *
     * @param currentGroupId
     * @param keyWord
     * @param request
     * @param session
     * @param response
     * @return
     */
    @RequiresPermissions("menu:wxuser")
    @RequestMapping(value = "/list")
    @AccountRequired
    public String toWxUserList(String currentGroupId, String keyWord, String flag, String message,
                               HttpServletRequest request, HttpSession session, HttpServletResponse response) throws Exception {
        if (!StringUtils.isEmpty(keyWord)) {
            currentGroupId = null;
        }
        if ("0".equals(flag)) {
            request.setAttribute("message", URLDecoder.decode(message, "UTF-8"));
        }
        request.setAttribute("currentGroupId", currentGroupId);
        request.setAttribute("search", keyWord);
        request.setAttribute("pager", wxUserManageService.getWxUserListByAccountId(new Pager<>(request, response),
                (Integer) session.getAttribute("currentAccoutId"), keyWord, currentGroupId));
        request.setAttribute("groups", wxUserManageService.getWeixinUserGroupByAccountId((Integer) session.getAttribute("currentAccoutId")));
        return "/home/wxmanage/base/wxUserManage/list";
    }

    /**
     * 更改用户所属组
     *
     * @param currentGroupId
     * @param groupId
     * @param userId
     * @param redirectAttributes
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:wxuser")
    @RequestMapping(value = "/changeGroup")
    public String changeUserGroup(String currentGroupId, String groupId, String userId,
                                  RedirectAttributes redirectAttributes) throws Exception {
        redirectAttributes.addAttribute("currentGroupId", currentGroupId);
        addMessage(redirectAttributes, SUCCESS, "操作成功!");
        wxUserManageService.updateUserGroup(groupId, userId);
        return "redirect:/wxmanager/wxuser/list";
    }

    /**
     * 批量更改用户所属组
     *
     * @param groupId
     * @param userSet
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequiresPermissions("menu:wxuser")
    @RequestMapping(value = "/batchChangeGroup")
    public String batchChangeUserGroup(String groupId, String userSet) throws Exception {
        wxUserManageService.updateUserGroupBatch(groupId, userSet);
        return "";
    }

    /**
     * 获取分组列表
     *
     * @param session
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:wxuser")
    @RequestMapping(value = "/groupList")
    @AccountRequired
    public String toGroupListPage(HttpSession session, HttpServletRequest request) throws Exception {
        request.setAttribute("groups", wxUserManageService.getGroupListByAccountId((Integer) session.getAttribute("currentAccoutId")));
        return "/home/wxmanage/base/wxUserManage/groupList";
    }

    /**
     * 保存、更新、删除组
     *
     * @param groupId
     * @param groupName
     * @param session
     * @param redirectAttributes
     * @return
     * @throws Exception
     */
    @RequiresPermissions("menu:wxuser")
    @RequestMapping("/edit")
    public String editGroup(String groupId, String groupName, HttpSession session, RedirectAttributes redirectAttributes) throws Exception {
        if (StringUtils.isEmpty(groupId)) {
            wxUserManageService.saveGroup(groupName, (Integer) session.getAttribute("currentAccoutId"));
            addMessage(redirectAttributes, SUCCESS, "保存成功!");
        } else {
            if (StringUtils.isEmpty(groupName)) {
                wxUserManageService.deleteGroup(groupId);
                addMessage(redirectAttributes, SUCCESS, "删除成功!");
            } else {
                wxUserManageService.updateGroup(groupId, groupName);
                addMessage(redirectAttributes, SUCCESS, "更新成功!");
            }
        }
        return "redirect:/wxmanager/wxuser/groupList";
    }

    @RequestMapping("/sign")
    public String register(WeixinUser weixinUser, String accountId, RedirectAttributes redirectAttributes) throws Exception {
        if (weixinUser.getRealname() != null && weixinUser.getPhoneNumber() != null && weixinUser.getOpenId() != null) {
            wxUserManageService.saveRegisterInfo(weixinUser, wxUserManageService.findWxUser(Integer.parseInt(accountId), weixinUser.getOpenId()));
        }
        addMessage(redirectAttributes, SUCCESS, "保存成功!");
        redirectAttributes.addAttribute("openId", weixinUser.getOpenId());
        redirectAttributes.addAttribute("accountId", accountId);
        return "redirect:/wxmanager/wxuser/register";
    }

    @RequestMapping("/register")
    public String toRegisterPage(String openId, String accountId, HttpServletRequest request) throws Exception {
        request.setAttribute("openId", openId);
        request.setAttribute("accountId", accountId);
        return "/mobile/wxUser/register";
    }
}
