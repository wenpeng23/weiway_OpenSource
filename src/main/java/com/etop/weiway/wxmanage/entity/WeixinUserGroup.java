package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Jeremie on 2015/1/14.
 */
@Entity
@Table(name = "t_weixin_user_group", schema = "", catalog = "weixin")
public class WeixinUserGroup extends BaseEntity {
    private Account account;
    private String name;
    private Integer count;
    private List<WeixinUser> weixinUsers;

    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(targetEntity = WeixinUser.class, fetch = FetchType.LAZY, mappedBy = "group")
    public List<WeixinUser> getWeixinUsers() {
        return weixinUsers;
    }

    public void setWeixinUsers(List<WeixinUser> weixinUsers) {
        this.weixinUsers = weixinUsers;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Transient
    public Integer getCount() {
        return this.count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WeixinUserGroup that = (WeixinUserGroup) o;

        if (!account.equals(that.account)) return false;
        if (!name.equals(that.name)) return false;
        if (!weixinUsers.equals(that.weixinUsers)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + account.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + weixinUsers.hashCode();
        return result;
    }
}
