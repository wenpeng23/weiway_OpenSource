package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_account", schema = "", catalog = "weixin")
public class Account extends BaseEntity {
    private String token;
    private String name;
    private String wechatId;
    private String portrait;
    private Short accountType;
    private String appId;
    private String appSecret;
    private User user;
    private String accessToken;
    private Long tokenCreatedTime;

    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "wechat_id")
    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId;
    }

    @Column(name = "portrait")
    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    @Column(name = "account_type")
    public Short getAccountType() {
        return accountType;
    }

    public void setAccountType(Short accountType) {
        this.accountType = accountType;
    }

    @Column(name = "app_id")
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Column(name = "app_secret")
    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    @Column(name = "access_token")
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Column(name = "token_created_time")
    public Long getTokenCreatedTime() {
        return tokenCreatedTime;
    }

    public void setTokenCreatedTime(Long tokenCreatedTime) {
        this.tokenCreatedTime = tokenCreatedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (id != account.id) return false;
        if (accountType != null ? !accountType.equals(account.accountType) : account.accountType != null) return false;
        if (appId != null ? !appId.equals(account.appId) : account.appId != null) return false;
        if (appSecret != null ? !appSecret.equals(account.appSecret) : account.appSecret != null) return false;
        if (name != null ? !name.equals(account.name) : account.name != null) return false;
        if (portrait != null ? !portrait.equals(account.portrait) : account.portrait != null) return false;
        if (token != null ? !token.equals(account.token) : account.token != null) return false;
        if (valid != null ? !valid.equals(account.valid) : account.valid != null) return false;
        if (wechatId != null ? !wechatId.equals(account.wechatId) : account.wechatId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (wechatId != null ? wechatId.hashCode() : 0);
        result = 31 * result + (portrait != null ? portrait.hashCode() : 0);
        result = 31 * result + (accountType != null ? accountType.hashCode() : 0);
        result = 31 * result + (appId != null ? appId.hashCode() : 0);
        result = 31 * result + (appSecret != null ? appSecret.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

    @JSONField(serialize = false)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
