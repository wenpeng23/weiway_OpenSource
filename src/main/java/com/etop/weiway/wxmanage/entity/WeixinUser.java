package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;

/**
 * Created by Jeremie on 2015/1/14.
 */
@Entity
@Table(name = "t_weixin_user", schema = "", catalog = "weixin")
public class WeixinUser extends BaseEntity {
    private Account account;
    private WeixinUserGroup group;
    private String openId;
    private Integer subscribe;
    private String nickname;
    private String realname;
    private String phoneNumber;
    private Integer sex;
    private String city;
    private String country;
    private String province;
    private String language;
    private String headimgurl;
    private Long subscribeTime;
    private String unionid;

    @JSONField(serialize = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @JSONField(serialize = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    public WeixinUserGroup getGroup() {
        return group;
    }

    public void setGroup(WeixinUserGroup group) {
        this.group = group;
    }

    @Column(name = "open_id")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Column
    public Integer getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Integer subscribe) {
        this.subscribe = subscribe;
    }

    @Column
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Column
    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Column
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    @Column
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Column
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Column
    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    @Column(name = "subscribe_time")
    public Long getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Long subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    @Column
    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WeixinUser that = (WeixinUser) o;

        if (!account.equals(that.account)) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (!group.equals(that.group)) return false;
        if (headimgurl != null ? !headimgurl.equals(that.headimgurl) : that.headimgurl != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (!nickname.equals(that.nickname)) return false;
        if (!openId.equals(that.openId)) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (province != null ? !province.equals(that.province) : that.province != null) return false;
        if (realname != null ? !realname.equals(that.realname) : that.realname != null) return false;
        if (sex != null ? !sex.equals(that.sex) : that.sex != null) return false;
        if (!subscribe.equals(that.subscribe)) return false;
        if (!subscribeTime.equals(that.subscribeTime)) return false;
        if (unionid != null ? !unionid.equals(that.unionid) : that.unionid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + account.hashCode();
        result = 31 * result + group.hashCode();
        result = 31 * result + openId.hashCode();
        result = 31 * result + subscribe.hashCode();
        result = 31 * result + nickname.hashCode();
        result = 31 * result + (realname != null ? realname.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (province != null ? province.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (headimgurl != null ? headimgurl.hashCode() : 0);
        result = 31 * result + subscribeTime.hashCode();
        result = 31 * result + (unionid != null ? unionid.hashCode() : 0);
        return result;
    }
}
