package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_material_news", schema = "", catalog = "weixin")
public class MaterialNews  extends BaseEntity implements Cloneable{
    private String name;
    private String mediaId;
    private String author;
    private String title;
    private String contentUrl;
    private String content;
    private String description;
    private Short showCoverPic;
    private Long createdTime;
    private MaterialImage thumbMedia;
    private Account account;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "media_id")
    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "content_url")
    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "show_cover_pic")
    public Short getShowCoverPic() {
        return showCoverPic;
    }

    public void setShowCoverPic(Short showCoverPic) {
        this.showCoverPic = showCoverPic;
    }

    @Column(name = "created_time")
    public Long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MaterialNews that = (MaterialNews) o;

        if (id != that.id) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (contentUrl != null ? !contentUrl.equals(that.contentUrl) : that.contentUrl != null) return false;
        if (createdTime != null ? !createdTime.equals(that.createdTime) : that.createdTime != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (mediaId != null ? !mediaId.equals(that.mediaId) : that.mediaId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (showCoverPic != null ? !showCoverPic.equals(that.showCoverPic) : that.showCoverPic != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (valid != null ? !valid.equals(that.valid) : that.valid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (mediaId != null ? mediaId.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (contentUrl != null ? contentUrl.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (showCoverPic != null ? showCoverPic.hashCode() : 0);
        result = 31 * result + (createdTime != null ? createdTime.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "thumb_media_id", referencedColumnName = "id")
    public MaterialImage getThumbMedia() {
        return thumbMedia;
    }

    public void setThumbMedia(MaterialImage thumbMedia) {
        this.thumbMedia = thumbMedia;
    }

    @JSONField(serialize = false)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        MaterialNews materialNews = new MaterialNews();
        materialNews.setId(this.id);
        materialNews.setAccount(this.account);
        materialNews.setAuthor(this.author);
        materialNews.setContent(this.content);
        materialNews.setContentUrl(this.contentUrl);
        materialNews.setName(this.name);
        materialNews.setMediaId(this.mediaId);
        materialNews.setTitle(this.title);
        materialNews.setDescription(this.description);
        materialNews.setShowCoverPic(this.showCoverPic);
        materialNews.setCreatedTime(this.createdTime);
        materialNews.setValid(this.valid);
        materialNews.setThumbMedia((MaterialImage)this.thumbMedia.clone());
        return materialNews;
    }
}
