package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_mass", schema = "", catalog = "weixin")
public class Mass  extends BaseEntity {
    private Long msgId;
    private String materialType;
    private Integer materialId;
    private String materialName;
    private String content;
    private String status;
    private Integer groupId;
    private String openIds;
    private Integer totalCount;
    private Integer filterCount;
    private Integer sentCount;
    private Integer errorCount;
    private Account account;

    @Column(name = "msg_id")
    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    @Column(name = "material_type")
    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    @Column(name = "material_id")
    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    @Column(name = "material_name")
    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "group_id")
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Column(name = "open_ids")
    public String getOpenIds() {
        return openIds;
    }

    public void setOpenIds(String openIds) {
        this.openIds = openIds;
    }

    @Column(name = "total_count")
    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @Column(name = "filter_count")
    public Integer getFilterCount() {
        return filterCount;
    }

    public void setFilterCount(Integer filterCount) {
        this.filterCount = filterCount;
    }

    @Column(name = "sent_count")
    public Integer getSentCount() {
        return sentCount;
    }

    public void setSentCount(Integer sentCount) {
        this.sentCount = sentCount;
    }

    @Column(name = "error_count")
    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Mass mass = (Mass) o;

        if (id != mass.id) return false;
        if (errorCount != null ? !errorCount.equals(mass.errorCount) : mass.errorCount != null) return false;
        if (filterCount != null ? !filterCount.equals(mass.filterCount) : mass.filterCount != null) return false;
        if (groupId != null ? !groupId.equals(mass.groupId) : mass.groupId != null) return false;
        if (materialId != null ? !materialId.equals(mass.materialId) : mass.materialId != null) return false;
        if (materialType != null ? !materialType.equals(mass.materialType) : mass.materialType != null) return false;
        if (msgId != null ? !msgId.equals(mass.msgId) : mass.msgId != null) return false;
        if (openIds != null ? !openIds.equals(mass.openIds) : mass.openIds != null) return false;
        if (sentCount != null ? !sentCount.equals(mass.sentCount) : mass.sentCount != null) return false;
        if (status != null ? !status.equals(mass.status) : mass.status != null) return false;
        if (totalCount != null ? !totalCount.equals(mass.totalCount) : mass.totalCount != null) return false;
        if (valid != null ? !valid.equals(mass.valid) : mass.valid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (msgId != null ? msgId.hashCode() : 0);
        result = 31 * result + (materialType != null ? materialType.hashCode() : 0);
        result = 31 * result + (materialId != null ? materialId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (groupId != null ? groupId.hashCode() : 0);
        result = 31 * result + (openIds != null ? openIds.hashCode() : 0);
        result = 31 * result + (totalCount != null ? totalCount.hashCode() : 0);
        result = 31 * result + (filterCount != null ? filterCount.hashCode() : 0);
        result = 31 * result + (sentCount != null ? sentCount.hashCode() : 0);
        result = 31 * result + (errorCount != null ? errorCount.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

    @JSONField(serialize = false)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
