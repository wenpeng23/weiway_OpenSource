package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_auto_reply", schema = "", catalog = "weixin")
public class AutoReply extends BaseEntity {
    private String keyWord;
    private Short keyType;
    private String materialType;
    private Integer materialId;
    private String materialName;
    private String content;
    private Integer orderId;
    private Account account;

    @Column(name = "key_word")
    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    @Column(name = "key_type")
    public Short getKeyType() {
        return keyType;
    }

    public void setKeyType(Short keyType) {
        this.keyType = keyType;
    }

    @Column(name = "material_type")
    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    @Column(name = "material_id")
    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    @Column(name = "material_name")
    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "order_id")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AutoReply autoReply = (AutoReply) o;

        if (id != autoReply.id) return false;
        if (keyType != null ? !keyType.equals(autoReply.keyType) : autoReply.keyType != null) return false;
        if (keyWord != null ? !keyWord.equals(autoReply.keyWord) : autoReply.keyWord != null) return false;
        if (materialId != null ? !materialId.equals(autoReply.materialId) : autoReply.materialId != null) return false;
        if (materialType != null ? !materialType.equals(autoReply.materialType) : autoReply.materialType != null)
            return false;
        if (orderId != null ? !orderId.equals(autoReply.orderId) : autoReply.orderId != null) return false;
        if (valid != null ? !valid.equals(autoReply.valid) : autoReply.valid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (keyWord != null ? keyWord.hashCode() : 0);
        result = 31 * result + (keyType != null ? keyType.hashCode() : 0);
        result = 31 * result + (materialType != null ? materialType.hashCode() : 0);
        result = 31 * result + (materialId != null ? materialId.hashCode() : 0);
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

    @JSONField(serialize = false)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
