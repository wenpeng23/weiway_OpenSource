package com.etop.weiway.wxmanage.entity;

import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_weixin_menu", schema = "", catalog = "weixin")
public class WeixinMenu  extends BaseEntity {
    private String title;
    private Short isParent;
    private String menuType;
    private String menuKey;
    private String url;
    private String materialType;
    private Integer materialId;
    private String materialName;
    private String content;
    private Integer orderId;
    private WeixinMenu parent;
    private List<WeixinMenu> subWeixinMenu;
    private Account account;

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "is_parent")
    public Short getIsParent() {
        return isParent;
    }

    public void setIsParent(Short isParent) {
        this.isParent = isParent;
    }

    @Column(name = "menu_type")
    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    @Column(name = "menu_key")
    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }

    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "order_id")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Column(name = "material_type")
    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    @Column(name = "material_id")
    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    @Column(name = "material_name")
    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WeixinMenu that = (WeixinMenu) o;

        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (isParent != null ? !isParent.equals(that.isParent) : that.isParent != null) return false;
        if (materialId != null ? !materialId.equals(that.materialId) : that.materialId != null) return false;
        if (materialName != null ? !materialName.equals(that.materialName) : that.materialName != null) return false;
        if (materialType != null ? !materialType.equals(that.materialType) : that.materialType != null) return false;
        if (menuKey != null ? !menuKey.equals(that.menuKey) : that.menuKey != null) return false;
        if (menuType != null ? !menuType.equals(that.menuType) : that.menuType != null) return false;
        if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null) return false;
        if (parent != null ? !parent.equals(that.parent) : that.parent != null) return false;
        if (subWeixinMenu != null ? !subWeixinMenu.equals(that.subWeixinMenu) : that.subWeixinMenu != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (isParent != null ? isParent.hashCode() : 0);
        result = 31 * result + (menuType != null ? menuType.hashCode() : 0);
        result = 31 * result + (menuKey != null ? menuKey.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (materialType != null ? materialType.hashCode() : 0);
        result = 31 * result + (materialId != null ? materialId.hashCode() : 0);
        result = 31 * result + (materialName != null ? materialName.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (subWeixinMenu != null ? subWeixinMenu.hashCode() : 0);
        result = 31 * result + (account != null ? account.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    public WeixinMenu getParent() {
        return parent;
    }

    public void setParent(WeixinMenu parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent",fetch = FetchType.LAZY)
    public List<WeixinMenu> getSubWeixinMenu() {
        return subWeixinMenu;
    }

    public void setSubWeixinMenu(List<WeixinMenu> subWeixinMenu) {
        this.subWeixinMenu = subWeixinMenu;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
