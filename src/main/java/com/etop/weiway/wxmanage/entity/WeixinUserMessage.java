package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Jeremie on 2015/1/14.
 */

@Entity
@Table(name = "t_weixin_user_message", schema = "", catalog = "weixin")
public class WeixinUserMessage extends BaseEntity {
    private Account account;
    private WeixinUser weixinUser;
    private String username;
    private String msgId;
    private String msgType;
    private String mediaId;
    private String thumbMediaId;
    private String content;
    private Integer haveReplied;
    private Long createTime;
    private List<CustomReply> replyMessages;

    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "weixin_user_id", referencedColumnName = "id")
    public WeixinUser getWeixinUser() {
        return weixinUser;
    }

    public void setWeixinUser(WeixinUser weixinUser) {
        this.weixinUser = weixinUser;
    }

    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "msg_id")
    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    @Column(name = "msg_type")
    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    @Column(name = "media_id")
    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    @Column(name = "thumb_media_id")
    public String getThumbMediaId() {
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "have_replied")
    public Integer getHaveReplied() {
        return haveReplied;
    }

    public void setHaveReplied(Integer haveReplied) {
        this.haveReplied = haveReplied;
    }

    @Column(name = "create_time")
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }


    @OneToMany(targetEntity = CustomReply.class, fetch = FetchType.LAZY, mappedBy = "replyMessage")
    public List<CustomReply> getReplyMessages() {
        return replyMessages;
    }

    public void setReplyMessages(List<CustomReply> replyMessages) {
        this.replyMessages = replyMessages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        WeixinUserMessage that = (WeixinUserMessage) o;

        if (!account.equals(that.account)) return false;
        if (!content.equals(that.content)) return false;
        if (!createTime.equals(that.createTime)) return false;
        if (!haveReplied.equals(that.haveReplied)) return false;
        if (mediaId != null ? !mediaId.equals(that.mediaId) : that.mediaId != null) return false;
        if (!msgId.equals(that.msgId)) return false;
        if (msgType != null ? !msgType.equals(that.msgType) : that.msgType != null) return false;
        if (thumbMediaId != null ? !thumbMediaId.equals(that.thumbMediaId) : that.thumbMediaId != null) return false;
        if (!username.equals(that.username)) return false;
        if (!weixinUser.equals(that.weixinUser)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + account.hashCode();
        result = 31 * result + weixinUser.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + msgId.hashCode();
        result = 31 * result + (msgType != null ? msgType.hashCode() : 0);
        result = 31 * result + (mediaId != null ? mediaId.hashCode() : 0);
        result = 31 * result + (thumbMediaId != null ? thumbMediaId.hashCode() : 0);
        result = 31 * result + content.hashCode();
        result = 31 * result + haveReplied.hashCode();
        result = 31 * result + createTime.hashCode();
        return result;
    }
}
