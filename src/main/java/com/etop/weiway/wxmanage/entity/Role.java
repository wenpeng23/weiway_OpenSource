package com.etop.weiway.wxmanage.entity;

import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_role", schema = "", catalog = "weixin")
public class Role extends BaseEntity {
    private String roleName;
    private List<Permission> permissions;

    @Column(name = "role_name")
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @OneToMany(targetEntity = Permission.class,fetch=FetchType.LAZY)
    @JoinTable(name = "t_role_permission", joinColumns = {@JoinColumn(name = "role_id")}, inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    @Transient
    public Set<String> getPermissionsName(){
        HashSet<String> permissionsNameSet = new HashSet<>();
        for(Permission permission:permissions){
            permissionsNameSet.add(permission.getPermissionName());
        }
        return permissionsNameSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (id != role.id) return false;
        if (roleName != null ? !roleName.equals(role.roleName) : role.roleName != null) return false;
        if (valid != null ? !valid.equals(role.valid) : role.valid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
        result = 31 * result + (valid != null ? valid.hashCode() : 0);
        return result;
    }

}
