package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;

/**
 * Created by Jeremie on 2015/1/14.
 */

@Entity
@Table(name = "t_custom_reply", schema = "", catalog = "weixin")
public class CustomReply extends BaseEntity{
    private Account account;
    private String openId;
    private WeixinUserMessage replyMessage;
    private String materialType;
    private Integer materialId;
    private String materialName;
    private String content;
    private String status;
    private Long createTime;

    @JSONField(serialize = false)
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Column(name = "open_id")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "reply_message_id", referencedColumnName = "id")
    public WeixinUserMessage getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(WeixinUserMessage replyMessage) {
        this.replyMessage = replyMessage;
    }

    @Column(name = "material_type")
    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    @Column(name = "material_id")
    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    @Column(name = "material_name")
    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "create_time")
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CustomReply that = (CustomReply) o;

        if (!account.equals(that.account)) return false;
        if (!createTime.equals(that.createTime)) return false;
        if (!materialId.equals(that.materialId)) return false;
        if (!materialName.equals(that.materialName)) return false;
        if (!materialType.equals(that.materialType)) return false;
        if (!openId.equals(that.openId)) return false;
        if (!replyMessage.equals(that.replyMessage)) return false;
        if (!status.equals(that.status)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + account.hashCode();
        result = 31 * result + openId.hashCode();
        result = 31 * result + replyMessage.hashCode();
        result = 31 * result + materialType.hashCode();
        result = 31 * result + materialId.hashCode();
        result = 31 * result + materialName.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + createTime.hashCode();
        return result;
    }
}
