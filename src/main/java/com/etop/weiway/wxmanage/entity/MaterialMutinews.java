package com.etop.weiway.wxmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.etop.weiway.basic.entity.BaseEntity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Entity
@Table(name = "t_material_mutinews", schema = "", catalog = "weixin")
public class MaterialMutinews  extends BaseEntity {
    private String name;
    private String multIds;
    private String mediaId;
    private Account account;
    private Long createdTime;
    private List<MaterialNews> materialNewses;

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "mult_ids")
    public String getMultIds() {
        return multIds;
    }

    public void setMultIds(String multIds) {
        this.multIds = multIds;
    }

    @Column(name = "media_id")
    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    @Column(name = "created_time")
    public Long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    @Transient
    public List<MaterialNews> getMaterialNewses() {
        return materialNewses;
    }

    public void setMaterialNewses(List<MaterialNews> materialNewses) {
        this.materialNewses = materialNewses;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MaterialMutinews that = (MaterialMutinews) o;

        if (!account.equals(that.account)) return false;
        if (!createdTime.equals(that.createdTime)) return false;
        if (materialNewses != null ? !materialNewses.equals(that.materialNewses) : that.materialNewses != null)
            return false;
        if (mediaId != null ? !mediaId.equals(that.mediaId) : that.mediaId != null) return false;
        if (!multIds.equals(that.multIds)) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + multIds.hashCode();
        result = 31 * result + (mediaId != null ? mediaId.hashCode() : 0);
        result = 31 * result + account.hashCode();
        result = 31 * result + createdTime.hashCode();
        result = 31 * result + (materialNewses != null ? materialNewses.hashCode() : 0);
        return result;
    }

    @JSONField(serialize = false)
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
