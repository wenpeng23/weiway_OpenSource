package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.WeixinUser;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2015/1/15.
 */
@Repository("WeixinUserDAO")
public class WeixinUserDAO extends BaseDao<WeixinUser> {
}
