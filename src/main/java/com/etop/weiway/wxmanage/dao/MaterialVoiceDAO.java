package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.MaterialVoice;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("MaterialVoiceDAO")
public class MaterialVoiceDAO extends BaseDao<MaterialVoice> {
}
