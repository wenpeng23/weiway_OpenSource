package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.basic.entity.BaseEntity;
import com.etop.weiway.wxmanage.entity.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("AccountDAO")
public class AccountDAO extends BaseDao<Account> {

}
