package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.CustomReply;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2015/1/15.
 */
@Repository("CustomReplyDAO")
public class CustomReplyDAO extends BaseDao<CustomReply> {
}
