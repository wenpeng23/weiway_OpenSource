package com.etop.weiway.wxmanage.dao;


import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.User;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/9/30.
 */
@Repository("UserDAO")
public class UserDAO extends BaseDao<User> {
}
