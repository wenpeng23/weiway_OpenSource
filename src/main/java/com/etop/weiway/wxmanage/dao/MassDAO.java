package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.Mass;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("MassDAO")
public class MassDAO extends BaseDao<Mass> {
}
