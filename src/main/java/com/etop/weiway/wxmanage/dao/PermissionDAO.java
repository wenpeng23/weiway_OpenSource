package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.Permission;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("PermissionDAO")
public class PermissionDAO extends BaseDao<Permission> {
}
