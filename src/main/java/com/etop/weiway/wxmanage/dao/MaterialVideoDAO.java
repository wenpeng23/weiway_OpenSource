package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.MaterialVideo;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("MaterialVideoDAO")
public class MaterialVideoDAO extends BaseDao<MaterialVideo> {
}
