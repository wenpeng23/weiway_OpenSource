package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.WeixinMenu;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("WeixinMenuDAO")
public class WeixinMenuDAO extends BaseDao<WeixinMenu> {

}
