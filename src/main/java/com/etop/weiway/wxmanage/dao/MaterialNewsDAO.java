package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.MaterialNews;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("MaterialNewsDAO")
public class MaterialNewsDAO extends BaseDao<MaterialNews> {

}
