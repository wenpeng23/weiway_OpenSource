package com.etop.weiway.wxmanage.dao;

import com.etop.weiway.basic.dao.BaseDao;
import com.etop.weiway.wxmanage.entity.Menu;
import org.springframework.stereotype.Repository;

/**
 * Created by Jeremie on 2014/12/8.
 */
@Repository("MenuDAO")
public class MenuDAO extends BaseDao<Menu> {
}
