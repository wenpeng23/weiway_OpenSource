package com.etop.weiway.wxmanage.service;

import com.etop.weiway.commons.web.Pager;
import com.etop.weiway.wxmanage.entity.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Jeremie on 2014/12/13.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;
    @Autowired
    private UserService userService;
    @Before
    public void init() {

    }

    @Test
    public void testLoadAccount(){
        System.out.println(accountService.loadAccount(1));
    }

    @Test
    public void testSaveAccount() throws Exception {
        Account account = new Account();
        account.setName("微信公众平台测试号");
        account.setUser(userService.findByName("jeremie"));
        account.setToken("weixin");
        account.setWechatId("test");
        account.setAccountType((short) 1);
        account.setAppId("test");
        account.setAppSecret("test");
        accountService.saveAccount(account,true);
        System.out.println(accountService.loadAccount(account.getId()).getAppId());
    }

    @Test
    public void testDeleteAccountById() throws Exception {
        accountService.deleteAccountById(2,false);
    }

    @Test
    public void testGetAccountByPage() throws Exception {
        Pager<Account> accouts= accountService.getAccountByPage(new Pager<>(1, 10), "",1);
        for(Account account:accouts.getList()){
            System.out.println(account.getName());
        }
    }
}
